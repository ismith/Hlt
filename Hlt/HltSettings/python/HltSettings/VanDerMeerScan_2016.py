from Configurables import HltConf

# No TURBO => no aferburner
HltConf().EnableHltAfterburner = False

class VanDerMeerScan_2016(object):
    """Settings for VDM scans and BGI in 2016.

    @author R. Matev
    @date 2016-02-25
    """

    def verifyType(self, ref):
        # verify self.ActiveLines is still consistent with
        # our types self.ActiveHlt2Lines and self.ActiveHlt1Lines...
        # so we can force that classes which inherit from us
        # and overrule either ActiveHlt.Lines also overrule
        # HltType...
        if (self.ActiveHlt1Lines() != ref.ActiveHlt1Lines(self) or
            self.ActiveHlt2Lines() != ref.ActiveHlt2Lines(self)):
            raise RuntimeError('Must update HltType when modifying ActiveHlt.Lines()')

    def L0TCK(self):
        return '0x1608'

    def HltType(self):
        self.verifyType(VanDerMeerScan_2016)
        return 'VanDerMeerScan_2016'

    def ActiveHlt1Lines(self):
        """Return a list of active Hlt1 lines."""
        lines = [
            # General lines
            'Hlt1Lumi',
            'Hlt1VeloClosingMicroBias',
            # Beam-gas lines
            'Hlt1BeamGasNoBeamBeam1',
            'Hlt1BeamGasNoBeamBeam2',
            'Hlt1BeamGasBeam1',
            'Hlt1BeamGasBeam2',
            'Hlt1BeamGasCrossingForcedReco',
            'Hlt1BeamGasCrossingForcedRecoFullZ',
        ]
        return lines

    def ActiveHlt2Lines(self):
        """Return a list of active Hlt2 Lines."""
        lines = [
            'Hlt2Lumi',
            'Hlt2BeamGas',
        ]
        return lines

    def Thresholds(self):
        """Return a dictionary of cuts."""

        from Hlt1Lines.Hlt1LumiLines import Hlt1LumiLinesConf
        from Hlt1Lines.Hlt1CommissioningLines import Hlt1CommissioningLinesConf
        from Hlt1Lines.Hlt1BeamGasLines import Hlt1BeamGasLinesConf

        thresholds = {
            Hlt1LumiLinesConf: {
                'Prescale': {'Hlt1Lumi': 1.},
                'Postscale': {'Hlt1Lumi': 1.},
            },
            Hlt1CommissioningLinesConf: {
                'Prescale': {'Hlt1VeloClosingMicroBias': 1},
                'Postscale': {'Hlt1VeloClosingMicroBias': 'RATE(500)'},
                'ODINVeloClosing': 'ODIN_BXTYP == LHCb.ODIN.BeamCrossing'
                # TODO do we really need the VeloClosing line on all BB triggers?
                #      Normally we have it only when VELO is open
            },
            Hlt1BeamGasLinesConf: {
                # Global behaviour settings
                'TrackingConf'          : 'FastVelo',
                'FitTracks'             : True,
                'PVFitter'              : 'LSAdaptPV3DFitter',
                'PVSeeding'             : 'PVSeed3DTool',
                'SplitVertices'         : True,
                'CreateGlobalSelection' : False,
                'Turbo'                 : False,
                'FullZVetoLumiTriggers' : False,
                'UseGEC'                : 'None',

                # Minimum number of tracks for the produced vertices (#tr/vtx > X)
                'VertexMinNTracks'          : 9,  # strictly greater than
                'FullZVertexMinNTracks'     : 27,  # strictly greater than

                # z-ranges for Vertexing
                'Beam1VtxRangeLow'        : -2000.,
                'Beam1VtxRangeUp'         :  2000.,
                'Beam2VtxRangeLow'        : -2000.,
                'Beam2VtxRangeUp'         :  2000.,
                # Luminous region exclusion range
                'BGVtxExclRangeMin'       :  -250.,
                'BGVtxExclRangeMax'       :   250.,

                # Take any L0 channel
                'L0Filter' : {
                    'NoBeamBeam1' : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'NoBeamBeam2' : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'Beam1'       : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'Beam2'       : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BB'          : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                },

                # No L0 rate limiters, prescales or postscales for calibration!
                'L0RateLimit' : {
                    'NoBeamBeam1'     : None,
                    'NoBeamBeam2'     : None,
                    'Beam1'           : None,
                    'Beam2'           : None,
                    'ForcedReco'      : None,
                    'ForcedRecoFullZ' : None,
                },
                'Prescale' : {
                    'Hlt1BeamGasNoBeamBeam1'             : 1.,
                    'Hlt1BeamGasNoBeamBeam2'             : 1.,
                    'Hlt1BeamGasBeam1'                   : 1.,
                    'Hlt1BeamGasBeam2'                   : 1.,
                    'Hlt1BeamGasCrossingForcedReco'      : 1.,
                    'Hlt1BeamGasCrossingForcedRecoFullZ' : 1.,
                },
                'Postscale' : {
                    'Hlt1BeamGasNoBeamBeam1'             : 1.,
                    'Hlt1BeamGasNoBeamBeam2'             : 1.,
                    'Hlt1BeamGasBeam1'                   : 1.,
                    'Hlt1BeamGasBeam2'                   : 1.,
                    'Hlt1BeamGasCrossingForcedReco'      : 1.,
                    'Hlt1BeamGasCrossingForcedRecoFullZ' : 1.,
                },
            },
        }

        # Hlt2 pass through lines
        from Hlt2Lines.Technical.Lines import TechnicalLines
        thresholds.update({
            TechnicalLines: {
                'Prescale': {
                    'Hlt2BeamGas': 1.,
                },
            }
        })
        return thresholds

    def Streams(self):
        return {
            # Deliberately turn off the LUMI stream to not double the large
            # rate of lumi during VDM (45 kHz) to storage
            'BEAMGAS': "HLT_PASS_SUBSTR('Hlt2BeamGas')",  # Hlt2Lumi is added by HltOutput
            'VELOCLOSING': None,
        }

    def NanoBanks(self):
        nanobanks = {'LUMI': ['ODIN', 'HltLumiSummary', 'HltRoutingBits', 'DAQ']}
        nanobanks['BEAMGAS'] = nanobanks['LUMI'] + ['Velo', 'L0DU', 'HltDecReports', 'HC']
        return nanobanks

    def StreamsWithLumi(self):
        return ['BEAMGAS']
