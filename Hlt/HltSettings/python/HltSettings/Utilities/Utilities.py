
def update_thresholds(current, extra):
    for conf, d in extra.iteritems():
        if conf not in current:
            current[conf] = d
            continue
        cur = current[conf]
        for k, v in d.iteritems():
            if k not in cur:
                cur[k] = v
                continue
            if cur[k] == v:
                continue
            print 'WARNING: potential collision detected: {} -- {}'.format(conf, k)
            print '         current:', cur[k]
            print '         request:', v
            if type(cur[k]) == dict:
                cur[k].update(v)
            else:
                cur[k] = v
            print 'result:', cur[k]
