import GaudiPython
from Gaudi.Configuration import*
from Configurables import ConfigStackAccessSvc, ConfigDBAccessSvc, ConfigTarFileAccessSvc, ConfigFileAccessSvc, ConfigTreeEditor, PropertyConfigSvc

from TCKUtils.utils import *


def replaceString(regExp,oldstring,repstr):

    import re
    matches = re.findall(regExp,oldstring)
    for m in matches:
        newstr = ''
        for n in m: newstr += n
        print "Modifying %s to %s" %(newstr,repstr)
        oldstring = oldstring.replace(newstr,repstr)

    return oldstring

def removePID( tck, newtck = None, 
               algname = '.*', property = 'DaughtersCuts|Combination.*Cut|MotherCut|Selection', 
               regExp = ['(\()( *)(PID)(e|mu|pi|K|p)( *)(<|>)( *)(-|)([0-9]*\.[0-9]*|[0-9]*\.|[0-9]*)( *)(\))',
                         '(\()( *)(\(|)( *)(PID)(e|mu|pi|K|p)( *)(-|)( *)(PID)(e|mu|pi|K|p)( *)(\)|)( *)(<|>)( *)([0-9]*\.[0-9]*|[0-9]*\.|[0-9]*)( *)(\))',
                         "(\()( *)(MINTREE)( *)(\()( *')([a-z]*[A-Z]*)([\+-]*)( *')( *)(==)( *)(ABSID)( *)(,)( *)(PID)(e|mu|pi|K|p)( *)(\))( *)(<|>)( *)(-|)( *)([0-9]*\.[0-9]*|[0-9]*\.|[0-9]*)( *)(\))"], 
               repString = '( ALL )', 
               dumpFile = '', cas = ConfigAccessSvc() ) :

    props = getProperties(tck,algname,property)
    newprops = dict()
    for p,d in props.iteritems():
        for k,c in d.iteritems():
            newc = c
            for rexp in regExp:
                newc = replaceString(rexp,newc,repString)
            if k=='Selection':
              print "Modifying %s to %s" % (c,'[""]')
              newc = '[""]'
            if c != newc:
              if not newprops.has_key(p):
                newprops[p] = {}
                newprops[p][k] = newc
              else:
                newprops[p][k] = newc


    from pprint import pprint
    print "==================> dump update <===================="
    pprint(newprops)
    label = None
    for (i,j) in getConfigurations().iteritems() :
       if tck in j['TCK'] : label = j['label']
    id = updateProperties( resolveTCK(tck),newprops,
                           label='%s - Same as 0x%08x without PID cuts' % (label,tck), 
                           cas=ConfigAccessSvc(Mode='ReadWrite'))
    createTCKEntries({newtck:id},cas=ConfigAccessSvc(Mode='ReadWrite'))

##
## main
##

if __name__ == "__main__":
    dataTCK = 0x010600a1
    print "Create PID-less TCK from TCK %x" % (dataTCK)
    removePID( tck = dataTCK, newtck = (dataTCK | 0xc0000000), 
               cas=ConfigAccessSvc(Mode='ReadWrite') )
