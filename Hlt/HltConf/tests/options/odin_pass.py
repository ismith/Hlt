from LoKiHlt.decorators import *
from LoKiCore.math import jbit
from HltConf.Configuration import _odin_pass_functor_code

exec(_odin_pass_functor_code)


assert repr(ODIN_PASS(LHCb.ODIN.Lumi)) == \
       repr(jbit(ODIN_EVTTYP, 3)), \
       'Failed test for creating a single event type filter'
assert repr(ODIN_PASS(LHCb.ODIN.Physics | LHCb.ODIN.NoBias | LHCb.ODIN.Lumi)) == \
       repr(jbit(ODIN_EVTTYP, 1) | jbit(ODIN_EVTTYP, 2) | jbit(ODIN_EVTTYP, 3)), \
       'Failed test for creating a multiple event type filter'

odin = LHCb.ODIN()
odin.setEventType(LHCb.ODIN.Physics | LHCb.ODIN.Lumi | LHCb.ODIN.SequencerTrigger)

assert ODIN_PASS(LHCb.ODIN.Lumi)(odin), \
       'Failed test for passing a single event type filter'
assert ODIN_PASS(LHCb.ODIN.NoBias | LHCb.ODIN.Lumi)(odin), \
       'Failed test for passing a multiple event type filter'
assert not ODIN_PASS(LHCb.ODIN.NoBias)(odin), \
       'Failed test for not passing a single event type filter'
