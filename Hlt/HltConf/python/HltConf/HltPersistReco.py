"""Configures the persistence of reconstructed objects."""

import itertools
from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from Configurables import GaudiSequencer as Sequence
from Configurables import HltPackedDataWriter

from GaudiConf.PersistRecoConf import PersistRecoPacking

from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking
from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedDownstreamTracking
from HltTracking.HltVertexNames import Hlt3DPrimaryVerticesName as PV3DSelection
from HltTracking.HltVertexNames import HltSharedVerticesPrefix, HltGlobalVertexLocation
from HltTracking.HltVertexNames import _vertexLocation
from HltTracking.HltPVs import PV3D

from Configurables import CaloProcessor, RichRecSysConf, ChargedProtoANNPIDConf

__author__ = "Sean Benson, Rosen Matev"


class HltPersistRecoConf(LHCbConfigurableUser):
    # python configurables to be applied before me
    __queried_configurables__ = [
        Hlt2BiKalmanFittedForwardTracking,
        Hlt2BiKalmanFittedDownstreamTracking,
    ]
    # python configurables that I configure
    __used_configurables__ = [
        (CaloProcessor, "SharedCaloProcessor"),
        (RichRecSysConf, "Hlt2LongTracking_RichRecSysConf"),
        (RichRecSysConf, "Hlt2DownstreamTracking_RichRecSysConf"),
        (ChargedProtoANNPIDConf, "Hlt2LongTracking_ChargedProtoANNPIDConf")
    ]

    __slots__ = {
        "Sequence":        None,
        "CaloHypoPrefix":  "Hlt2/PID/CALO/Calo",
        "OutputLevel":     WARNING,
    }

    def __apply_configuration__(self):
        """Apply the HLT persist reco configuration."""

        from Configurables import GaudiSequencer
        from Configurables import LoKi__HDRFilter
        from Configurables import TrackToDST
        from Configurables import MoveRecVertexTracks

        persistRecoSeq = self.getProp("Sequence")
        if not self.getProp("Sequence"):
            return
        persistRecoSeq.IgnoreFilterPassed = False
        persistRecoSeq.Members = []

        # This sequence expects the proper line filter to be applied already

        # Cut down states in tracks first
        tracks = Hlt2BiKalmanFittedForwardTracking().hlt2PrepareTracks()
        tracksDown = Hlt2BiKalmanFittedDownstreamTracking().hlt2PrepareTracks()
        longStateCutter = TrackToDST("Hlt2TrackToDSTLong")
        longStateCutter.TracksInContainer = tracks.outputSelection()
        downStateCutter = TrackToDST("Hlt2TrackToDSTDown")
        downStateCutter.TracksInContainer = tracksDown.outputSelection()
        persistRecoSeq.Members += [longStateCutter]
        persistRecoSeq.Members += [downStateCutter]

        # Remove all tracks that don't belong to a PV (to save space in the raw bank)
        movePVTracks = MoveRecVertexTracks("Hlt2MovePVTracks")
        movePVTracks.VertexLocation = PV3D("Hlt2").output
        movePVTracks.OutputLocation = self.__pvTrackLocation()
        persistRecoSeq.Members += [movePVTracks]

        fittedVeloTracksCutter = TrackToDST("Hlt2TrackToDSTVeloPV", veloStates=["ClosestToBeam"])
        fittedVeloTracksCutter.TracksInContainer = self.__pvTrackLocation()
        persistRecoSeq.Members += [fittedVeloTracksCutter]

        # Setup packers and add them to the sequence
        packing = self.__packing()
        packerAlgs = packing.packers()
        for alg in packerAlgs:
            alg.AlwaysCreateOutput = True
            alg.DeleteInput = False
            alg.OutputLevel = self.getProp("OutputLevel")
        persistRecoSeq.Members += packerAlgs

        # Configure HltPackedDataWriter algorithm to add to the raw banks
        pdwriter = HltPackedDataWriter("Hlt2PackedDataWriter")
        pdwriter.PackedContainers = packing.packedLocations()
        pdwriter.ContainerMap = packing.inputToPackedLocationMap()
        persistRecoSeq.Members += [pdwriter]

        # Register the mapping of output locations and integers
        self.__registerToHltANNSvc(packing.packedLocations() +
                                   packing.externalLocations())

    def __registerToHltANNSvc(self, locations):
        """Register the packed object locations in the HltANNSvc."""
        from Configurables import HltANNSvc
        location_ids = {loc: i+1 for i, loc in enumerate(locations)}
        HltANNSvc().PackedObjectLocations = location_ids

    def __pvTrackLocation(self):
        # NOTE we need to import FittedVelo here, otherwise the FastVeloTracking
        #      instance is created before any defaults are overridden!
        from HltTracking.HltSharedTracking import FittedVelo
        return FittedVelo.outputSelection() + 'InPV'

    def __packing(self):
        """Return a PersistRecoPacking object."""
        pvLocation = _vertexLocation(HltSharedVerticesPrefix, HltGlobalVertexLocation, PV3DSelection)
        longTracking = Hlt2BiKalmanFittedForwardTracking()
        downstreamTracking = Hlt2BiKalmanFittedDownstreamTracking()
        caloHypoPrefix = self.getProp("CaloHypoPrefix")

        inputs = {
            "Hlt2LongProtos":                longTracking.hlt2ChargedAllPIDsProtos().outputSelection(),
            "Hlt2DownstreamProtos":          downstreamTracking.hlt2ChargedNoPIDsProtos().outputSelection(),
            "Hlt2LongRichPIDs":              longTracking.hlt2RICHID().outputSelection(),
            "Hlt2DownstreamRichPIDs":        downstreamTracking.hlt2RICHID().outputSelection(),
            "Hlt2LongMuonPIDs":              longTracking.hlt2MuonID().outputSelection(),
            "Hlt2DownstreamPIDMuonSegments": downstreamTracking._trackifiedMuonIDLocation(),
            "Hlt2LongTracks":                longTracking.hlt2PrepareTracks().outputSelection(),
            "Hlt2DownstreamTracks":          downstreamTracking.hlt2PrepareTracks().outputSelection(),
            "Hlt2VeloPVTracks":              self.__pvTrackLocation(),
            "Hlt2RecVertices":               pvLocation,
            "Hlt2NeutralProtos":             longTracking.hlt2NeutralProtos().outputSelection(),
            "Hlt2CaloClusters":              "Hlt/Calo/EcalClusters",
            "Hlt2EcalSplitClusters":         caloHypoPrefix + "/EcalSplitClusters",
            "Hlt2CaloElectronHypos":         caloHypoPrefix + "/Electrons",
            "Hlt2CaloPhotonHypos":           caloHypoPrefix + "/Photons",
            "Hlt2CaloMergedPi0Hypos":        caloHypoPrefix + "/MergedPi0s",
            "Hlt2CaloSplitPhotonHypos":      caloHypoPrefix + "/SplitPhotons",
        }
        for k in inputs:
            if not inputs[k].startswith('/Event/'):
                inputs[k] = '/Event/' + inputs[k]

        return PersistRecoPacking(inputs=inputs)
